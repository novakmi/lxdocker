/* (c) Michal Novák, LxDockerPlugin it.novakmi@gmail.com, see LICENSE file */

package com.github.novakmi.lxdocker.test

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.testng.Assert
import org.testng.annotations.AfterMethod
import org.testng.annotations.BeforeMethod
import org.testng.annotations.Test

import java.lang.reflect.Method

//@Slf4j
class LxDockerfileTaskTest {

    File testProjectDir
    private File settingsFile
    private File buildFile
    final String TAG="lxdocker"

    @BeforeMethod(alwaysRun = true)
    public void setup(Method method) {
        testProjectDir = new File("${System.getProperty("testTmpDir")}/${method.getName()}")
        if (testProjectDir.exists()) {
            testProjectDir.deleteDir()
        }
        Assert.assertTrue testProjectDir.mkdirs()

        settingsFile = new File("${testProjectDir.getAbsolutePath()}/settings.gradle")
        buildFile = new File("${testProjectDir.getAbsolutePath()}/build.gradle")
        settingsFile.createNewFile()
        buildFile.createNewFile()
        settingsFile << "rootProject.name = '${method.getName()}'"
    }

    @AfterMethod(alwaysRun = true)
    public void deleteTempDir() {

    }

    @Test(groups = ["basic"])
    public void minimal() {
        buildFile <<
            "plugins {id 'lxdocker'}\n" +
            "import com.github.novakmi.lxdocker.LxDockerfileTask\n" +
            "\n" +
            "task createLinuxDevDockerfile(type: LxDockerfileTask) {\n" +
            "   tag = getProp(\"IMAGE_TAG_LX\", \"$TAG\")\n" +
            "}\n"

        BuildResult result = GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withPluginClasspath()
            .withArguments("createLinuxDevDockerfile")
            .build()
        List lines = TestUtils.getDockerFileLines(testProjectDir, TAG)
        Assert.assertEquals(lines.size(), (int) 1) //one for last empty line
        Assert.assertTrue(TestUtils.contains(lines, "FROM ubuntu:22.04"))
    }

    @Test(groups = ["basic"])
    public void from() {
        buildFile <<
            "plugins {id 'lxdocker'}\n" +
            "import com.github.novakmi.lxdocker.LxDockerfileTask\n" +
            "\n" +
            "task createLinuxDevDockerfile(type: LxDockerfileTask) {\n" +
            "   from getProp(\"BASE_IMAGE_TAG_LX\", \"ubuntu:19.10\")\n" +
            "   tag = getProp(\"IMAGE_TAG_LX\", \"$TAG\")\n" +
            "}\n"

        BuildResult result = GradleRunner.create()
            .withProjectDir(testProjectDir)
            .withPluginClasspath()
            .withArguments("createLinuxDevDockerfile")
            .build()

        List lines = TestUtils.getDockerFileLines(testProjectDir, TAG)
        Assert.assertEquals(lines.size(), (int) 1) //one for last empty line
        Assert.assertTrue(TestUtils.contains(lines, "FROM ubuntu:19.10"))
    }
}
